import redis
import logging


class Config(object):
    """项目配置信息"""

    # 配置mysql数据库
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 设置自动提交
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    # 配置redis
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # 配置密钥
    SECRET_KEY = "cjFZwA5TDVNPWyysPsDok4QqwNh+1dNH6nL36VTa+Eo="

    # 配置Flask_session
    SESSION_TYPE = "redis"
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    SESSION_USE_SIGNER = True
    PERMANENT_SESSION_LIFETIME = 86400 * 7

    # 配置debug模式
    DEBUG = True


class ProductionConfig(Config):
    """生产模式"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING


class DevelopmentConfig(Config):
    """开发模式"""
    DEBUG = True
    LOG_LEVEL = logging.DEBUG
