from info.models import User
from info import create_app, db
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from config import DevelopmentConfig, ProductionConfig
# 导入模型类文件
from info import models

# 调用工厂方法创建app实例
app = create_app(DevelopmentConfig)

# 创建manager对象,拥有命令行
manager = Manager(app)

# 实例化Migrate迁移对象
Migrate(app, db)

# 添加manager绑定迁移的命令-->命令行增加db命令 以及迁移指令
manager.add_command('db', MigrateCommand)


# 添加创建管理员命令函数
@manager.option("-n", "-name", dest="name")
@manager.option("-p", "-password", dest="password")
def createsuperuser(name, password):
    """创建管理员用户"""
    if not all([name, password]):
        print(" 参数不足")
        return

    # 创建用户并设置 admin 属性
    user = User()
    user.nick_name = name
    user.mobile = name
    user.password = password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()

    print("添加成功")


if __name__ == '__main__':
    print(app.url_map)
    manager.run()
