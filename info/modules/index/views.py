"""
首页相关视图
"""
from . import index_blue
from info import constants
from info.utils.response_code import RET
from info.models import User, News, Category
from info.utils.common import user_login_data
from flask import render_template, current_app, session, request, jsonify, g

"""
实现首页新闻列表路由
请求方式:GET
参数:cid分类ID, page页码, per_page每页新闻条数
三个都可为空
"""


@index_blue.route('/news_list')
def get_news_list():
    # 1. 获取参数
    category_id = request.args.get('cid', '1')
    page = request.args.get('page', '1')
    per_page = request.args.get('per_page', '10')

    # 2. 校验参数
    try:
        # 判断传入的是否int/float可以使用强行转格式
        category_id = int(category_id)
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数传入错误")

    # 3. 查询数据库
    filters = [News.status == 0]  # 添加通过审核查询条件
    # 这是一个通用的处理可选参数的方法,如果用户传入了就给列表添加
    if category_id != 1:
        filters.append(News.category_id == category_id)

    # 使用paginate (页码,每页多少条,出错是否会抛出)
    # *filter 可以将查询条件列表展开
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询错误")

    # 获取查询后的数据
    news_model_list = paginate.items

    # 获取分页的总页码
    total_page = paginate.pages

    # 获取当前页码
    current_page = paginate.page

    # 遍历转换为字典格式
    news_dict_li = []
    for news in news_model_list:
        news_dict_li.append(news.to_basic_dict())

    data = {
        "news_dict_li": news_dict_li,
        "total_page": total_page,
        "current_page": current_page
    }

    return jsonify(errno=RET.OK, errmsg="OK", data=data)


@index_blue.route('/')
@user_login_data
def index():

    # 点击排行数据 -->查询数据库 -->将模型转换为字典传入模板中
    news_list = []

    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.looger.error(e)

    click_news_list = []
    for news in news_list:
        click_news_list.append(news.to_basic_dict())

    # 三. 分类数据 --> 查询数据库 --> 传入模板
    categories = Category.query.all()

    category_li = []
    for category in categories:
        category_li.append(category.to_dict())

    # 3. 返回数据
    data = {
        "user": g.user.to_dict() if g.user else None,
        "click_news_list": click_news_list,
        "category_li": category_li
    }
    return render_template('news/index.html', data=data)


@index_blue.route('/favicon.ico')
def favicon():
    # 发送静态文件
    # current_app可以理解为app的替身,能够直接运行app的相关函数
    # send_static_file:专门用于发送静态文件(图像/文本/js)
    # current_app是在一个请求中,自动产生的,是app的代理对象
    return current_app.send_static_file('news/favicon.ico')
