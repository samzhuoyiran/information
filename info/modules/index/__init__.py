# 1.导入蓝图
from flask import Blueprint

# 2.创建蓝图对象
index_blue = Blueprint('index', __name__)

# 2.1 导入子模块
from . import views
