"""
通行证相关视图
"""
import re
import random

from . import passport_blu
from info.models import User
from datetime import datetime
from info.libs.yuntongxun.sms import CCP
from info.utils.response_code import RET
from info import redis_store, constants, db
from info.utils.captcha.captcha import captcha
from flask import request, current_app, make_response, jsonify, session


@passport_blu.route('/logout', methods=['POST'])
def logout():
    """
    退出登录就是清除session中的对应登录之后保存的信息
    """
    # 使用session.pop()进行删除对应的信息
    session.pop("user_id", None)
    session.pop("nick_name", None)
    session.pop("mobile", None)
    session.pop("is_admin", None)  # 需要清空是否管理员的相关数据

    # 返回结果
    return jsonify(errno=RET.OK, errmsg="OK")


"""
实现登录路由
URL:/passport/login
参数:mobile,password
"""


@passport_blu.route('/login', methods=['POST'])
def login():
    """
    1. 获取参数
    2. 校验参数:手机号格式
    3. 判断手机号是否注册过:查询数据库
    4. 判断用户是否存在 or 判断密码是否正确
    设置最后登录时间--数据库自动提交
    5. 设置用户登录的session信息
    6. 返回成功信息
    """
    # 1. 获取参数
    json_dict = request.json
    mobile = json_dict.get('mobile')
    password = json_dict.get('password')

    # 2. 校验参数:手机号格式
    if not all([mobile, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")

    if not re.match(r'1[3-9]\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号错误")

    # 3. 判断手机号是否注册过:查询数据库
    try:
        user = User.query.filter_by(mobile=mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

    # 4. 判断用户是否存在 or 判断密码是否正确
    if not user:
        # 用户不存在
        return jsonify(errno=RET.NODATA, errmsg="该手机号尚未注册")
    if not user.check_passowrd(password):
        return jsonify(errno=RET.PWDERR, errmsg="密码错误")

    # 5. 设置用户登录的session信息
    session['user_id'] = user.id
    session['mobile'] = user.mobile
    session['nick_name'] = user.nick_name

    # 设置最后的登录时间 ---> 进行默认提交设置
    user.last_login = datetime.now()

    # 6. 返回成功信息
    return jsonify(errno=RET.OK, errmsg="登陆成功")


"""
实现注册路由
URL:/passport/register
参数:mobile,sms_code,password
"""


@passport_blu.route('/register', methods=['POST'])
def register():
    """
    一. 获取参数
    二. 校验参数
    三. 逻辑处理
    四. 返回信息
    """

    # 1. 获取参数:mobile,sms_code,password
    json_dict = request.json
    mobile = json_dict.get('mobile')
    sms_code = json_dict.get('sms_code')
    password = json_dict.get('password')

    # 2. 校验参数:完整性,手机号
    if not all([mobile, sms_code, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    if not re.match(r'1[3-9]\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号错误")

    # 3. 从Redis中取出短信验证码进行对比,并判断是否过期
    try:
        real_sms_code = redis_store.get('SMS_' + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询失败")

    if not real_sms_code:
        return jsonify(errno=RET.NODATA, errmsg="短信验证吗已过期")

    if sms_code != real_sms_code:
        return jsonify(errno=RET.DATAERR, errmsg="短信验证码错误")

    # 在查询之后需要删除短信验证码(对比完了就没用了,虽然过期就自动删除,但是为了保险(安全/逻辑完善))
    try:
        redis_store.delete('SMS_' + mobile)
    except Exception as e:
        current_app.logger.error(e)

    # 4. 对比成功,注册用户(1.判断手机号是否注册过;2.创建user模型;3.添加到数据库)
    try:
        user = User.query.filter_by(mobile=mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

    if user:
        # 用户已存在
        return jsonify(errno=RET.DATAEXIST, errmsg="该手机号已经注册")

    # 创建user模型
    user = User()
    user.mobile = mobile
    user.nick_name = mobile
    # 用户密码需要进行加盐加密,在模型类文件中已经完成函数封装
    user.password = password

    # 添加到数据库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        # 数据库的修改操作,失败了要回滚
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="添加到数据库失败")

    # 5. 实现登录 -->设置session
    session['user_id'] = user.id
    session['nick_name'] = user.nick_name
    session['mobile'] = user.mobile

    # 6. 返回成功信息
    return jsonify(errno=RET.OK, errmsg="注册成功")


"""
实现获取图片验证码的路由/接口
URL:/passport/image_code?image_code_id=xxxxxxxx
参数:image_code_id
"""


@passport_blu.route('/image_code')
def get_image_code():
    # 1. 获取参数图片验证码id
    image_code_id = request.args.get('image_code_id')

    # 2. 生成验证码图片,调用SDK接口
    name, text, image_data = captcha.generate_captcha()

    # 3. 存储到redis缓存数据库中(设置过期时间常量)
    try:
        redis_store.set('Image_code_id' + image_code_id, text, constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        # 记录错误到日志中
        current_app.logger.error(e)
        # 返回错误信息给前端
        return make_response(jsonify(errno=RET.DATAERR, errmsg='保存图片验证码失败'))

    # 4. 返回图片
    response = make_response(image_data)
    response.headers['Content-Type'] = 'image/jpg'
    return response


"""
实现短信验证码的获取
RUL:/passport/sms_code
参数:mobile, image_code, image_code_id
"""


@passport_blu.route('/sms_code', methods=['POST'])
def get_sms_code():
    # 代码步骤分析
    # 1. 获取参数(手机号/图片验证码/验证码id)
    params_dict = request.json
    mobile = params_dict.get('mobile')
    image_code = params_dict.get('image_code')
    image_code_id = params_dict.get('image_code_id')

    # 2. 进行校验,校验数据的完整性和正则匹配手机号
    if not all([mobile, image_code_id, image_code]):
        return make_response(jsonify(errno=RET.PARAMERR, errmsg='参数不完整'))

    if not re.match(r'1[3-9][0-9]{9}', mobile):
        return make_response(jsonify(errno=RET.PARAMERR, errmsg='输入的手机号有误'))

    # 3. 处理逻辑
    # 先从数据库读取数据(需要判断是否数据)数据库操作使用try
    try:
        real_image_code = redis_store.get('Image_code_id' + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据失败')

    # 4. 对用户传入的数据与数据库进行对比,若是失败返回错误信息
    # 先判断redis中数据有没有过期
    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg='验证码已过期')

    if real_image_code.upper() != image_code.upper():
        return jsonify(errno=RET.DATAERR, errmsg='验证码错误')

    # 5. 对比一致的话就生成验证码值(第三方只负责发短信,具体值需要我们传入),保存到redis缓存数据库
    sms_code_str = '%06d' % random.randint(0, 999999)
    current_app.logger.error(sms_code_str)

    # 6. 调用SDK发送短信
    # result = CCP().send_template_sms(mobile, [sms_code_str, constants.SMS_CODE_VALIDITY], 1)

    # if result != 0:
    #     # 表名发送失败
    #     return jsonify(errno=RET.THIRDERR, errmsg='短信发送失败')

    # 保存到redis
    try:
        redis_store.set('SMS_' + mobile, sms_code_str, constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='短信验证码保存到redis失败')

    # 7. 返回数据
    return jsonify(errno=RET.OK, errmsg='短信验证码发送成功')
