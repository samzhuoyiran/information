"""
个人中心模块
"""
from flask import Blueprint

# 创建passport蓝图对象
profile_blu = Blueprint('profile', __name__, url_prefix='/user')

from . import views
