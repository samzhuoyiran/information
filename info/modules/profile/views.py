"""
个人中心相关视图
"""
import re
import random

from . import profile_blu
from info.models import User, News, Category
from datetime import datetime
from info.utils.response_code import RET
from info import redis_store, constants, db
from info.utils.image_storage import storage
from info.utils.common import user_login_data
from flask import request, current_app, jsonify, session, render_template, g, redirect, abort


# 其他用户界面新闻列表路由
@profile_blu.route('/other_news_list')
def other_news_list():

    # 获取参数
    p = request.args.get("p",1)
    user_id = request.args.get("user_id")

    # 校验参数
    if not all([p, user_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数是错误")

    # 查询该用户
    try:
        user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询错误")
    
    if not user:
        return jsonify(errno=RET.NODATA, errmsg="无此用户")
    
    # 查询该用户所有的新闻列表
    news_li = []
    current_page = 1
    total_page = 1
    try:
        paginate = News.query.filter(News.user_id==user.id).paginate(p, constants.OTHER_NEWS_PAGE_MAX_COUNT, False)
        news_li = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询错误")

    news_dict_li = []
    for news in news_li:
        news_dict_li.append(news.to_review_dict())

    data = {
        "news_list": news_dict_li,
        "current_page":current_page,
        "total_page":total_page
    }
    return jsonify(errno=RET.OK, errmsg="OK", data=data)


# 其他用户信息界面路由
@profile_blu.route('/other_info')
@user_login_data
def other_info():
    # 获取用户 ID
    other_user_id = request.args.get("user_id")

    if not other_user_id:
        abort(404)

    # 查询该用户
    other_user = None  # type:User
    try:
        other_user = User.query.get(other_user_id)
    except Exception as e:
        current_app.logger.error(e)

    if not other_user:
        abort(404)

    # 判断当前用户是否关注了该用户
    is_followed = False
    if g.user:
        if other_user.followers.filter(User.id == g.user.id).count() > 0:
            is_followed = True

    data = {
        "user": g.user.to_dict(),
        "other_info": other_user.to_dict(),
        "is_followed": is_followed
    }

    return render_template("news/other.html", data=data)


# 我的关注路由
@profile_blu.route('/user_follow')
@user_login_data
def user_follow():
    # 获取页数
    p = request.args.get("p", 1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user
    follows = []
    current_page = 1
    total_page = 1
    try:
        paginate = user.followed.paginate(p, constants.USER_FOLLOWED_MAX_COUNT, False)
        follows = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    follows_list = []
    for follow in follows:
        follows_list.append(follow.to_dict())

    data = {
        "users": follows_list,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("news/user_follow.html", data=data)


# 用户发布新闻列表
@profile_blu.route('/news_list')
@user_login_data
def news_list():
    # 查询新闻  需要自己的 ID
    user = g.user
    p = request.args.get("p", 1)

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="页码错误")

    news_model_list = []
    current_page = 1
    total_page = 1

    # 查询数据库获取数据
    try:
        paginate = News.query.filter(News.user_id == user.id).paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
        news_model_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

    news_li = []
    for new in news_model_list:
        news_li.append(new.to_review_dict())

    data = {
        "news_list": news_li,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("news/user_news_list.html", data=data)


# 用户发布新闻接口
@profile_blu.route('/news_release', methods=["GET", "POST"])
@user_login_data
def news_release():
    if request.method == "GET":

        # 返回分类数据给前端
        categories = []

        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

        # 定义分类列表保存
        category_list = []
        for category in categories:
            category_list.append(category.to_dict())

        # 需要移除最新
        category_list.pop(0)

        data = {
            "categories": category_list
        }

        return render_template("news/user_news_release.html", data=data)

    # 处理 POST 提交,执行发布新闻操作

    # 获取提交的数据
    title = request.form.get("title")
    source = "个人发布"
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")

    # 进行校验
    if not all([title, source, digest, content, index_image, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    try:
        category_id = int(category_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 添加图片
    try:
        index_image_data = index_image.read()
        key = storage(index_image_data)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="上传图片失败")

    # 建立News模型添加到数据表
    news = News()
    news.title = title
    news.source = source
    news.digest = digest
    news.content = content
    news.index_image_url = constants.QINIU_DOMIN_PREFIX + key
    news.category_id = category_id
    news.user_id = g.user.id
    # 需要添加一个审核状态
    news.status = 1

    try:
        db.session.add(news)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库错误")
    return jsonify(errno=RET.OK, errmsg="OK")


# 用户新闻收藏列表
@profile_blu.route('/collection')
@user_login_data
def collection():
    # 获取页数,默认为1,并对其进行校验
    p = request.args.get("p", 1)

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user

    collection_model_li = []
    current_page = 1
    total_page = 1

    try:
        # 分页数据查询
        paginate = user.collection_news.paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)

        # 获取查询的分页数据
        collection_model_li = paginate.items

        # 获取当前页码
        current_page = paginate.page

        # 获取总页码
        total_page = paginate.pages

    except Exception as e:
        current_app.logger.error(e)

    collection_list = []
    for news in collection_model_li:
        collection_list.append(news.to_basic_dict())

    data = {
        "total_page": total_page,
        "current_page": current_page,
        "collections": collection_list
    }

    return render_template("news/user_collection.html", data=data)


# 修改密码路由接口
@profile_blu.route('/pass_info', methods=['GET', 'POST'])
@user_login_data
def pass_info():
    # 获取登录用户信息
    user = g.user
    if request.method == "GET":
        return render_template("news/user_pass_info.html")

    # 处理POST请求
    # 获取参数
    json_dict = request.json
    old_password = json_dict.get("old_password")
    new_password = json_dict.get("new_password")

    # 校验参数
    if not all([old_password, new_password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")

    # 获取用户的原始密码,对比成功进行修改
    if not user.check_passowrd(old_password):
        return jsonify(errno=RET.PWDERR, errmsg="密码错误")

    user.password = new_password

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="修改失败")

    # 修改成功后登出,需要重新登录
    session.pop("user_id", None)
    session.pop("nick_name", None)
    session.pop("mobile", None)

    return jsonify(errno=RET.OK, errmsg="OK")


# 用户头像上传路由接口
@profile_blu.route('/pic_info', methods=['GET', 'POST'])
@user_login_data
def pic_info():
    # 获取登录用户信息
    user = g.user
    if request.method == "GET":
        return render_template("news/user_pic_info.html", data={"user": user.to_dict()})

    # 处理POST请求
    # 获取参数
    try:
        avatar = request.files.get("avatar").read()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="读取文件错误")

    # 上传图像
    try:
        key = storage(avatar)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")

    user.avatar_url = key

    return jsonify(errno=RET.OK, errmsg="OK", data={"avatar_url": constants.QINIU_DOMIN_PREFIX + key})


# 个人中心内嵌网页视图
@profile_blu.route('/base_info', methods=["GET", "POST"])
@user_login_data
def base_info():
    # 获取登录用户的信息
    user = g.user
    # 对GET请求方式进行处理
    if request.method == "GET":
        return render_template("news/user_base_info.html", data={"user": user.to_dict()})

    # 对POST请求进行处理
    # 获取参数
    json_dict = request.json
    nick_name = json_dict.get("nick_name")
    gender = json_dict.get("gender")
    signature = json_dict.get("signature")

    # 校验参数
    if not all([nick_name, gender, signature]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")

    if gender not in ("MAN", "WOMAN"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 更新保存数据
    user.nick_name = nick_name
    user.gender = gender
    user.signature = signature

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="资料保存失败")

    # 需要更新session的nick_name
    session["nick_name"] = user.nick_name

    return jsonify(errno=RET.OK, errmsg="OK")


# 个人中心视图
@profile_blu.route('/info')
@user_login_data
def get_user_info():
    # 获取登录用户信息
    user = g.user

    # 如果用户没登录则返回首页
    if not user:
        return redirect('/')

    data = {
        "user": user.to_dict()
    }

    return render_template("news/user.html", data=data)
