"""
新闻相关视图
"""
from . import news_blu
from info import constants, db
from info.models import News, User, Comment, CommentLike
from info.utils.response_code import RET
from info.utils.common import user_login_data
from flask import render_template, current_app, session, g, abort, request, jsonify


# 关注与取消关注接口
@news_blu.route('/followed_user', methods=["POST"])
@user_login_data
def followed_user():

    # 对用户登录判断
    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 获取参数 & 校验
    user = g.user
    user_id = request.json.get("user_id")
    action = request.json.get("action")

    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    if action not in ("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询目标用户信息
    target_user = None  # type:User
    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg="用户不存在")

    # 根据不同操作做不同逻辑
    if action == "follow":
        if target_user.followers.filter(User.id==user.id).count() > 0:
            return jsonify(errno=RET.DATAEXIST, errmsg="用户已关注")
        target_user.followers.append(user)
    else:
        if target_user.followers.filter(User.id==user.id).count() > 0:
            target_user.followers.remove(user)

    # 提交到数据库
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据提交错误")

    return jsonify(errno=RET.OK, errmsg="OK")


@news_blu.route('/comment_like', methods=["POST"])
@user_login_data
def set_comment_like():
    """评论点赞"""

    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 获取参数
    comment_id = request.json.get("comment_id")
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    # 判断参数
    if not all([comment_id, news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("add", "remove"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询评论数据
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg="评论数据不存在")

    if action == "add":
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if not comment_like:
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = g.user.id
            db.session.add(comment_like)
            # 增加点赞条数
            comment.like_count += 1
    else:
        # 删除点赞数据
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if comment_like:
            db.session.delete(comment_like)
            # 减小点赞条数
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="操作失败")

    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blu.route('/news_comment', methods=['POST'])
@user_login_data
def news_comment():
    """
    实现新闻评论后端
    URL: /news/news_comment
    参数: news_id(必需), comment(必需), parent_id(非)
    """

    # 1. 获取参数
    user = g.user
    json_dict = request.json
    news_id = json_dict.get("news_id")
    comment_str = json_dict.get("comment")
    parent_id = json_dict.get("parent_id")

    # 2. 校验参数 完整性
    if not all([news_id, comment_str]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    # 判断新闻数据是否有值
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询失败")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="该新闻已过期")

    # 3. 处理逻辑 创建评论模型->添加到数据库
    comment = Comment()
    comment.news_id = news_id
    comment.user_id = user.id
    comment.content = comment_str

    if parent_id:
        # 如果有父评论ID则添加
        comment.parent_id = parent_id

    # 因为自动提交是在请求结束后提交,这时的模型数据已返回,前端无法获取ID等数据
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存评论失败")

    return jsonify(errno=RET.OK, errmsg="ok", data=comment.to_dict())


"""
收藏&取消按钮点击
"""


@news_blu.route('/news_collect', methods=['POST'])
@user_login_data
def news_collect():
    # 1. 获取数据
    user = g.user
    json_dict = request.json
    news_id = json_dict.get("news_id")
    action = json_dict.get("action")

    # 2. 数据校验 --> 用户是否登录, 完整性
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    if action not in ("collect", "cancel_collect"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 3. 逻辑处理
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库失败")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻已过期")

    if action == "cancel_collect":
        if news in user.collection_news:  # 若是查询到新闻,而操作是取消收藏则执行
            user.collection_news.remove(news)
    else:
        if news not in user.collection_news:  # 若是查询到新闻,而操作是收藏则执行
            user.collection_news.append(news)

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存失败")

    return jsonify(errno=RET.OK, errmsg="ok")


"""
新闻详情路由
"""


@news_blu.route('/<int:news_id>')
@user_login_data
def get_news(news_id):
    # 一. 点击排行数据展示
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    # 二. 详情页新闻数据展示
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        abort(404)

    if not news:
        # 没有数据返回未找到页面
        abort(404)

    # 新闻的点击量需要+1
    news.clicks += 1

    # 当前用户是否关注该新闻作者
    is_followed = False
    # 四. 收藏信息
    is_collected = False  # if 用户已登录: 查询是否有该条收藏 有则改为true

    if g.user:
        if news in g.user.collection_news:
            is_collected = True

        if news.user.followers.filter(User.id==g.user.id).count() > 0:
            is_followed = True


    # 五. 当前新闻评论列表数据
    comment_model_li = []
    try:
        comment_model_li = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询评论失败")

    # 所有评论是否点赞数据
    comment_like_ids = []
    if g.user:
        # 若果当前用户已登录
        try:
            comment_ids = [comment.id for comment in comment_model_li]
            if len(comment_ids) > 0:
                # 取到当前用户在当前新闻的所有评论点赞的记录
                comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),
                                                         CommentLike.user_id == g.user.id).all()
                # 取出记录中所有评论的ID
                comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

        comment_list = []
        for item in comment_model_li if comment_model_li else []:
            comment_dict = item.to_dict()
            comment_dict["is_like"] = False
            # 判断用户是否点赞该评论
            if g.user and item.id in comment_like_ids:
                comment_dict["is_like"] = True
            comment_list.append(comment_dict)

    comment_list = []
    for comment in comment_model_li:
        comment_list.append(comment.to_dict())

    data = {
        "click_news_list": news_dict_li,
        "user": g.user.to_dict() if g.user else None,
        "news": news.to_dict(),
        "is_collected": is_collected,
        "comments": comment_list,
        "is_followed": is_followed
    }

    return render_template('news/detail.html', data=data)
