# 1.导入蓝图
from flask import Blueprint, session, request, url_for, redirect

# 2.创建蓝图对象
admin_blu = Blueprint('admin', __name__, url_prefix="/admin")

# 2.1 导入子模块
from . import views


# 控制后台管理权限
@admin_blu.before_request
def before_request():
    # 获取管理员信息
    is_admin = session.get("is_admin", False)

    # 进行判断筛选访问的页面不是登录界面
    if not is_admin and not request.url.endswith(url_for("admin.admin_login")):
        return redirect('/')
