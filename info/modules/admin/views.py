"""
后台管理相关视图
"""
import re
import random
import time

from . import admin_blu
from info.models import User, News, Category
from datetime import datetime, timedelta
from info.utils.response_code import RET
from info import redis_store, constants, db
from info.utils.image_storage import storage
from info.utils.common import user_login_data
from flask import request, current_app, jsonify, session, render_template, g, redirect, url_for


# 登出路由
@admin_blu.route('/logout', methods=["POST"])
@user_login_data
def admin_logout():

    # 清除登录的 session 和 cookie 信息
    session.pop("nick_name", None)
    session.pop("user_id", None)
    session.pop("mobile", None)
    session.pop("is_admin", None)

    # 重定向到管理元登录界面
    return redirect("/")


# 新闻分类管理
@admin_blu.route('/news_type', methods=["GET", "POST"])
def news_type():
    if request.method == "GET":
        categories = []
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
        if not categories:
            return render_template("admin/news_type.html", errmsg="数据查询失败")

        categories_list = []
        for category in categories:
            categories_list.append(category.to_dict())

        return render_template("admin/news_type.html", data={"categories": categories_list})

    # 处理新增或修改名称逻辑
    # 获取参数
    category_id = request.json.get("id")
    category_name = request.json.get("name")

    if not category_name:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 判断如果有分类 id 则是修改 name
    if category_id:
        category = None
        try:
            category = Category.query.get(category_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询错误")
        category.name = category_name

    else:
        # 没有 id 则是新增分类
        category = Category()
        category.name = category_name

    # 数据库提交
    try:
        db.session.add(category)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="操作失败")
    return jsonify(errno=RET.OK, errmsg="OK")


# 新闻版式编辑详情
@admin_blu.route('/news_edit_detail', methods=["GET", "POST"])
def news_edit_detail():
    if request.method == "GET":

        news_id = request.args.get("news_id")
        if not news_id:
            return render_template("admin/news_edit_detail.html", data={"errmsg": "未查询到此新闻"})

        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        if not news:
            return render_template("admin/news_edit_detail.html", data={"errmsg": "未查询到此新闻"})

        # 查询分类信息
        category_model_li = Category.query.all()
        category_list = []
        for category in category_model_li:
            c_dict = category.to_dict()
            c_dict["is_selected"] = False
            if category.id == news.category_id:
                c_dict["is_selected"] = True
            category_list.append(c_dict)

        # 移除最新分类
        category_list.pop(0)
        data = {
            "news": news.to_dict(),
            "categories": category_list
        }

        return render_template("admin/news_edit_detail.html", data=data)

    # 处理 POST 请求, 修改新闻内容
    # 获取参数
    news_id = request.form.get("news_id")
    title = request.form.get("title")
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")

    # 校验参数
    if not all([title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")

    # 查询该新闻
    news = None  # type:News
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到该新闻")

    # 读取图片数据
    if index_image:
        try:
            index_image_data = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="读取图片失败")

        # 将图片上传到存储平台
        try:
            key = storage(index_image_data)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key

    # 设置相关数据
    news.title = title
    news.digest = digest
    news.content = constants
    news.category_id = category_id

    # 数据库提交
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="上传数据错误")

    return jsonify(errno=RET.OK, errmsg="OK")


# 新闻版式列表路由
@admin_blu.route('/news_edit')
def news_edit():
    p = request.args.get("p", 1)
    keywords = request.args.get("keywords")
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    news_model_list = []
    current_page = 1
    total_page = 1

    filters = []
    if keywords:
        filters.append(News.title.contains(keywords))
    try:
        paginate = News.query.filter(*filters) \
            .order_by(News.create_time.desc()) \
            .paginate(p, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)
        news_model_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_list = []
    for news in news_model_list:
        news_list.append(news.to_basic_dict())

    data = {
        "news_list": news_list,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("admin/news_edit.html", data=data)


# 新闻审核详情路由
@admin_blu.route('/news_review_detail', methods=["GET", "POST"])
def news_review_detail():
    if request.method == "GET":

        news_id = request.args.get("news_id")
        if not news_id:
            return render_template("admin/news_review_detail.html", data={"errmsg": "未查到此新闻"})

        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        if not news:
            return render_template("admin/news_review_detail.html", data={"errmsg": "未查到此新闻"})

        data = {"news": news.to_dict()}

        return render_template("admin/news_review_detail.html", data=data)

    # 处理 POST 请求

    news_id = request.json.get("news_id")
    action = request.json.get("action")

    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不完整")
    if action not in ["accept", "reject"]:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    news = None

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news_id:
        return jsonify(errno=RET.NODATA, errmsg="未查询到此新闻")

    if action == "accept":
        news.status = 0
    else:
        reason = request.json.get("reason")
        if not reason:
            return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
        news.status = -1
        news.reason = reason

    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据保存失败")

    return jsonify(errno=RET.OK, errmsg="OK")


# 新闻审核路由
@admin_blu.route('/news_review')
def news_review():
    # 分页查询未审核 news 进行返回
    p = request.args.get("p", 1)
    # 添加关键字查询
    keywords = request.args.get("keywords")

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    news_model_list = []
    current_page = 1
    total_page = 1

    filters = [News.status != 0]
    if keywords:
        filters.append(News.title.contains(keywords))

    try:
        paginate = News.query.filter(*filters) \
            .order_by(News.create_time.desc()) \
            .paginate(p, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)
        news_model_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    news_list = []
    for news in news_model_list:
        news_list.append(news.to_review_dict())

    data = {
        "news_list": news_list,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("admin/news_review.html", data=data)


# 用户统计路由
@admin_blu.route('/user_list')
def admin_user_list():
    # 获取查询页数 -- 默认为1, 进行校验
    p = request.args.get("p", 1)

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    # 设置变量的默认值
    users = []
    current_page = 1
    total_page = 1

    # 使用分页查询数据库
    try:
        paginate = User.query.filter(User.is_admin == False) \
            .order_by(User.last_login.desc()) \
            .paginate(p, constants.ADMIN_USER_PAGE_MAX_COUNT, False)
        users = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="用户数据查询失败")

    users_list = []
    for user in users:
        users_list.append(user.to_admin_dict())
    data = {
        "users_list": users_list,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("admin/user_list.html", data=data)


# 用户统计路由
@admin_blu.route('/user_count')
def admin_user_count():
    # 用户总数
    total_count = 0
    try:
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)

    # 月新增数
    mon_count = 0
    t = time.localtime()
    begin_mon_date = datetime.strptime(("%d-%02d-01" % (t.tm_year, t.tm_mon)), "%Y-%m-%d")
    try:
        mon_count = User.query.filter(User.is_admin == False, User.create_time > begin_mon_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 日新增人数
    day_count = 0
    begin_day_count = datetime.strptime(("%d-%02d-%02d" % (t.tm_year, t.tm_mon, t.tm_mday)), "%Y-%m-%d")
    try:
        begin_day_count = User.query.filter(User.is_admin == False, User.create_time > begin_day_count).count()
    except Exception as e:
        current_app.logger.error(e)

    # 拆线图数据

    active_time = []
    active_count = []
    # 取到今天的时间字符串
    today_date_str = ("%d-%02d-%02d" % (t.tm_year, t.tm_mon, t.tm_mday))
    # 转为时间对象
    today_date = datetime.strptime(today_date_str, "%Y-%m-%d")
    for i in range(0, 31):
        # 取到某一天的0点0分
        begin_date = today_date - timedelta(days=i)
        # 取某一天的前一天
        end_date = today_date - timedelta(days=(i - 1))
        count = User.query.filter(User.is_admin == False, User.last_login >= begin_date,
                                  User.last_login < end_date).count()
        active_count.append(count)
        active_time.append(begin_date.strftime("%Y-%m-%d"))

    # 反转将最近一天放在最后面
    active_time.reverse()
    active_count.reverse()

    data = {
        "total_count": total_count,
        "mon_count": mon_count,
        "day_count": day_count,
        "active_time": active_time,
        "active_count": active_count
    }

    return render_template('admin/user_count.html', data=data)


# 后台首页界面路由
@admin_blu.route('/index')
@user_login_data
def admin_index():
    user = g.user.to_dict() if g.user else None
    return render_template("admin/index.html", user=user)


# 后台登录界面
@admin_blu.route('/login', methods=["GET", "POST"])
def admin_login():
    if request.method == "GET":
        # 从 session 中获取指定的值
        user_id = session.get("user_id", None)
        is_admin = session.get("is_admin", False)
        # 若果用户存在,并且是管理员则直接跳转到首页
        if user_id and is_admin:
            return redirect(url_for('admin.admin_index'))

        return render_template("admin/login.html")

    # 处理 POST 登录请求
    # 获取参数并进行校验完整性
    username = request.form.get("username")
    password = request.form.get("password")
    if not all([username, password]):
        return render_template("admin/login.html", errmsg="参数不全")

    # 处理登录逻辑
    try:
        user = User.query.filter(User.mobile == username).first()
    except Exception as e:
        current_app.logger.error(e)
        return render_template("admin/login.html", errmsg="查询失败")

    if not user:
        return render_template("admin/login.html", errmsg="用户不存在")

    if not user.check_passowrd(password):
        return render_template("admin/login.html", errmsg="密码错误")

    # 需要添加一个 is_admin 验证
    if not user.is_admin:
        return render_template("admin/login.html", errmsg="用户权限错误")

    # 通过验证进行 session 设置

    session["user_id"] = user.id
    session["nick_name"] = user.nick_name
    session["mobile"] = user.mobile
    session["is_admin"] = True

    # 跳转到后台首页
    return redirect(url_for('admin.admin_index'))
