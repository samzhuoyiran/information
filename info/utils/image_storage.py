from flask import current_app
from qiniu import Auth, put_data

access_key = '6HpJXhnT1MS70c7GjT--UrvRn6sMsxwDkIQ1fYQq'
secret_key = 'rn0V8J7trKklJwTRA8arYoFFCOe6OftoCt_w-s-4'

# 执行添加操作的函数
def storage(data):

    # 构建鉴权对象 --> 网络请求
    try:
        q = Auth(access_key, secret_key)
    except Exception as e:
        current_app.logger.error(e)
        raise e

    # 要上传的空间名称
    bucket_name = 'itheimaihome'

    # 生成上传token, 可以设置过期时间
    # 文件名可以不传,服务器自动帮我们生成
    token = q.upload_token(bucket_name, None, 3600)

    ret, info = put_data(token, None, data)

    if info.status_code != 200:
        raise Exception('上传图片失败!')

    return ret["key"]


if __name__ == '__main__':
    file = input('请输入文件路径')
    with open(file, 'rb') as f:
        storage(f.read())