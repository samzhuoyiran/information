"""
公用的工具类
"""
import functools

from flask import session, g


def do_index_class(index):
    """返回指定索引对应的类名"""
    if index == 0:
        return 'first'
    elif index == 1:
        return 'second'
    elif index == 2:
        return 'third'
    else:
        return ''


def user_login_data(f):
    """用户登录数据的装饰器"""

    # 让返回的函数名为外层传入的函数名
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        # 获取到当前登录用户的ID

        user_id = session.get("user_id")
        # 通过用户ID获取用户信息
        user = None

        if user_id:
            from info.models import User
            user = User.query.get(user_id)

        # 使用g变量进行存储
        g.user = user
        return f(*args, **kwargs)

    return wrapper
