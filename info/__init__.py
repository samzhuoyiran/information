# info 是整个程序编码的地方
import redis, logging

from logging.handlers import RotatingFileHandler
from flask import Flask, render_template, g
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect, generate_csrf
from flask_session import Session
from info.utils.common import do_index_class


# 先创建 SQLAlchemy 对象,先不传 app 实例
db = SQLAlchemy()

# 创建空的 redis 对象
redis_store = None  # type:redis.StrictRedis


def setup_log(config_name):
    # 设置日志的记录等级
    logging.basicConfig(level=config_name.LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


# 提供工厂类方法,方便参数的设置
def create_app(config_name):
    app = Flask(__name__)

    # 配置日志
    setup_log(config_name)

    # 导入项目配置
    app.config.from_object(config_name)

    # 调用SQLAlchemy对象并调用init_app方法,完成实例
    # 大多数扩展都支持先创建,再执行init_app方法来创建实例
    db.init_app(app)

    # 创建redis存储对象
    global redis_store
    redis_store = redis.StrictRedis(host=config_name.REDIS_HOST, port=config_name.REDIS_PORT, decode_responses=True)

    # 开启CSRFProtect保护
    CSRFProtect(app)

    # 对每一次请求生成csrf_token并传给前端
    @app.after_request
    def after_request(response):
        # 生成csrf_token的值
        csrf_token = generate_csrf()

        # 通过cookie传给前端
        response.set_cookie("csrf_token", csrf_token)

        return response

    # 增加自定义过滤器
    app.add_template_filter(do_index_class, "index_class")

    # 创建Flask-Session扩展
    # 作用: 会将存放在浏览器的cookie中的Session数据,同步到你指定的地方(服务器的redis地址)
    Session(app)

    # 4.在app创建的地方,注册蓝图
    # 在实际开发中,蓝图的导入很容易出现循环导包,需要用到时再导入
    from info.modules.index import index_blue
    app.register_blueprint(index_blue)

    from info.modules.passport import passport_blu
    app.register_blueprint(passport_blu)

    from info.modules.news import news_blu
    app.register_blueprint(news_blu)

    from info.modules.profile import profile_blu
    app.register_blueprint(profile_blu)

    from info.modules.admin import admin_blu
    app.register_blueprint(admin_blu)

    # 添加一个404界面
    from info.utils.common import user_login_data

    @app.errorhandler(404)
    @user_login_data
    def page_not_found(e):
        # 获取用户登录信息
        user = g.user
        data = {
            "user": user.to_dict() if user else None
        }
        return render_template("news/404.html", data=data)

    return app
